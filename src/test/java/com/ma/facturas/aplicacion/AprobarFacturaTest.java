package com.ma.facturas.aplicacion;

import com.ma.facturas.dominio.FacturaMother;
import com.ma.facturas.dominio.FacturaRepository;
import com.ma.shared.dominio.FacturaId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
class AprobarFacturaTest {

    @MockBean
    private FacturaRepository repository;

    @Autowired
    private AprobarFactura aprobarFactura;

    @Test
    void deberiaAprobarFactura() {
        when(repository.consultar(any())).thenReturn(FacturaMother.ejemplo());
        aprobarFactura.aprobar(FacturaId.random());
    }

}