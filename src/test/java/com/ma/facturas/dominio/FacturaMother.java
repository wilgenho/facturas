package com.ma.facturas.dominio;

import com.ma.shared.dominio.FacturaId;
import com.ma.shared.dominio.Importe;
import com.ma.shared.dominio.Moneda;
import com.ma.shared.dominio.ProveedorId;

import java.math.BigDecimal;
import java.time.LocalDate;

public class FacturaMother {

    public static Factura ejemplo() {
        return new Factura(
                FacturaId.random(),
                new Comprobante(1, "A", 100),
                new ProveedorId(1001),
                LocalDate.now(),
                new Importe(Moneda.ARS, new BigDecimal("5000.50"))
        );
    }
}
