package com.ma.shared.dominio;

public enum Moneda {
    ARS,
    EUR,
    USD
}
