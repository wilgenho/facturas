package com.ma.shared.dominio;

public class ProveedorId {

    private int id;

    public ProveedorId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
