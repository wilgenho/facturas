package com.ma.shared.dominio;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class Importe {

    Moneda moneda;
    BigDecimal cantidad;

    public Importe(Moneda moneda, BigDecimal cantidad) {
        if (moneda == null) {
            throw new IllegalArgumentException("La moneda es nula");
        }
        if (cantidad == null) {
            throw new IllegalArgumentException("La cantidad es nula");
        }

        this.moneda = moneda;
        this.cantidad = cantidad;
    }

    public static Importe cero (Moneda moneda) {
        return new Importe(moneda, BigDecimal.ZERO);
    }

    public Importe sumar(Importe valor) {
        if (!valor.moneda.equals(moneda)) {
            throw new IllegalArgumentException("Deben coincidir las monedas");
        }
        return new Importe(moneda, cantidad.add(valor.cantidad));
    }

}
