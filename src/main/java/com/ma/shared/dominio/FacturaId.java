package com.ma.shared.dominio;

import java.util.UUID;

public class FacturaId {

    private String id;

    public FacturaId(String id) {
        UUID uuid = UUID.fromString(id);
        this.id = uuid.toString();
    }

    public String getId() {
        return id;
    }

    public static FacturaId random() {
        return new FacturaId(UUID.randomUUID().toString());
    }
}
