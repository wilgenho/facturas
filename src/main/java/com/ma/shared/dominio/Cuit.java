package com.ma.shared.dominio;

public class Cuit {

    public static final long DIGITO_TIPO = 10000000000L;
    public static final int TIPO_EMPRESA = 3;
    private Long valor;

    public Cuit(Long valor) {
        validar(valor);
        this.valor = valor;
    }

    private void validar(Long valor) {
        if (valor == 0L) {
            throw new IllegalArgumentException("Cuit no valido");
        }
    }

    public boolean esEmpresa() {
        return valor / DIGITO_TIPO == TIPO_EMPRESA;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }
}
