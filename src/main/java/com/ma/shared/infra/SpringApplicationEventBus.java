package com.ma.shared.infra;

import com.ma.shared.dominio.eventos.DomainEvent;
import com.ma.shared.dominio.eventos.EventBus;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Log4j2
public class SpringApplicationEventBus implements EventBus {

    private final ApplicationEventPublisher publisher;

    public SpringApplicationEventBus(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public void publish(List<DomainEvent> events) {
        events.forEach(this::publish);
    }

    private void publish(final DomainEvent event) {
        log.info("Se produce el evento " + event.eventName());
        publisher.publishEvent(event);
    }
}
