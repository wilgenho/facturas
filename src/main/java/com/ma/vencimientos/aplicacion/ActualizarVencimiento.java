package com.ma.vencimientos.aplicacion;

import com.ma.shared.dominio.Importe;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@Log4j2
public class ActualizarVencimiento {

    public void actualizar(Importe importe, LocalDate fecha) {
        log.info("Actualizo vencimiento a la fecha {}, importe: {}" , fecha, importe);
    }
}
