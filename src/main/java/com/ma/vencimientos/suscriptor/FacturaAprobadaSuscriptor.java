package com.ma.vencimientos.suscriptor;

import com.ma.facturas.dominio.FacturaAprobada;
import com.ma.vencimientos.aplicacion.ActualizarVencimiento;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Log4j2
public class FacturaAprobadaSuscriptor {

    private final ActualizarVencimiento actualizarVencimiento;

    public FacturaAprobadaSuscriptor(final ActualizarVencimiento actualizarVencimiento) {
        this.actualizarVencimiento = actualizarVencimiento;
    }

    @EventListener
    public void onFacturaAprobada(FacturaAprobada facturaAprobada) {
        log.info("Factura {} aprobada" , facturaAprobada.getFacturaId());
        actualizarVencimiento.actualizar(facturaAprobada.getTotal(), facturaAprobada.getVencimiento());
    }

}
