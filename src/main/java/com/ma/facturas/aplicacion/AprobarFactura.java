package com.ma.facturas.aplicacion;

import com.ma.facturas.dominio.Factura;
import com.ma.facturas.dominio.FacturaRepository;
import com.ma.shared.dominio.FacturaId;
import com.ma.shared.dominio.eventos.EventBus;
import org.springframework.stereotype.Service;

@Service
public class AprobarFactura {

    private final FacturaRepository facturaRepository;
    private final EventBus eventBus;

    public AprobarFactura(final FacturaRepository facturaRepository, final EventBus eventBus) {
        this.facturaRepository = facturaRepository;
        this.eventBus = eventBus;
    }

    public void aprobar(FacturaId id) {
        Factura factura = facturaRepository.consultar(id);
        factura.aprobar();
        facturaRepository.guardar(factura);
        eventBus.publish(factura.pullDomainEvents());
    }
}
