package com.ma.facturas.aplicacion;

import com.ma.facturas.dominio.Concepto;
import com.ma.facturas.dominio.Factura;
import com.ma.facturas.dominio.FacturaRepository;
import com.ma.facturas.dominio.LineaFacturaId;
import com.ma.shared.dominio.FacturaId;
import com.ma.shared.dominio.Importe;
import org.springframework.stereotype.Service;

@Service
public class AgregarLineaFactura {

    private final FacturaRepository facturaRepository;

    public AgregarLineaFactura(final FacturaRepository facturaRepository) {
        this.facturaRepository = facturaRepository;
    }

    public void agregarLinea(FacturaId facturaId, Concepto concepto, Importe importe) {
        Factura factura = facturaRepository.consultar(facturaId);
        factura.crearLinea(LineaFacturaId.getRandom(), concepto, importe);
        facturaRepository.guardar(factura);
    }

}
