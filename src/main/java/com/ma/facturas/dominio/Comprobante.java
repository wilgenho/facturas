package com.ma.facturas.dominio;

public class Comprobante {

    private int puntoVenta;
    private String letra;
    private int numero;

    public Comprobante(int puntoVenta, String letra, int numero) {
        validarLetra(letra);
        this.puntoVenta = puntoVenta;
        this.letra = letra;
        this.numero = numero;
    }

    public int getPuntoVenta() {
        return puntoVenta;
    }

    public String getLetra() {
        return letra;
    }

    public int getNumero() {
        return numero;
    }

    private void validarLetra(String letra) {
        if (!letra.matches("[ABCM]")) {
            throw new IllegalArgumentException(String.format("Letra %s no valida", letra));
        }
    }

}
