package com.ma.facturas.dominio;

import com.ma.shared.dominio.FacturaId;

public interface FacturaRepository {
    Factura consultar(FacturaId id);
    void guardar(Factura factura);
}
