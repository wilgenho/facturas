package com.ma.facturas.dominio;

import lombok.Value;

@Value
public class Concepto {
   String valor;
}
