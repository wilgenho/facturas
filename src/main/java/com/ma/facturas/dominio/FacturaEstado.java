package com.ma.facturas.dominio;

public enum FacturaEstado {
    PENDIENTE,
    APROBADA,
    AUTORIZADA,
    ANULADA
}
