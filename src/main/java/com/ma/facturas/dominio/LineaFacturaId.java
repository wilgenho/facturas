package com.ma.facturas.dominio;

import java.util.UUID;

public class LineaFacturaId {

    String id;

    public LineaFacturaId(String id) {
        UUID uuid = UUID.fromString(id);
        this.id = uuid.toString();
    }

    public String getId() {
        return id;
    }

    public static LineaFacturaId getRandom() {
        return new LineaFacturaId(UUID.randomUUID().toString());
    }
}
