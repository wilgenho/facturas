package com.ma.facturas.dominio;

import com.ma.shared.dominio.Importe;
import lombok.NonNull;

import java.util.Objects;

public class LineaFactura {

    private final LineaFacturaId id;
    private final Concepto concepto;
    private final Importe importe;

    public LineaFactura(@NonNull LineaFacturaId id, @NonNull Concepto concepto, @NonNull Importe importe) {
        this.id = id;
        this.concepto = concepto;
        this.importe = importe;
    }

    public LineaFacturaId getId() {
        return id;
    }

    public Concepto getConcepto() {
        return concepto;
    }

    public Importe getImporte() {
        return importe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LineaFactura)) return false;
        LineaFactura that = (LineaFactura) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
