package com.ma.facturas.dominio;

import com.ma.shared.dominio.Importe;
import com.ma.shared.dominio.eventos.DomainEvent;
import lombok.Value;

import java.math.BigDecimal;
import java.time.LocalDate;

public class FacturaAprobada extends DomainEvent {

    private String facturaId;
    private String estado;
    private LocalDate vencimiento;
    private Importe total;
    private Integer proveedorId;

    public FacturaAprobada(String facturaId, String estado, LocalDate vencimiento, Importe total, Integer proveedorId) {
        super(facturaId);
        this.facturaId = facturaId;
        this.estado = estado;
        this.vencimiento = vencimiento;
        this.total = total;
        this.proveedorId = proveedorId;
    }

    @Override
    public String eventName() {
        return "factura.aprobada";
    }

    public String getFacturaId() {
        return facturaId;
    }

    public String getEstado() {
        return estado;
    }

    public LocalDate getVencimiento() {
        return vencimiento;
    }

    public Importe getTotal() {
        return total;
    }

    public Integer getProveedorId() {
        return proveedorId;
    }
}
