package com.ma.facturas.dominio;

import com.ma.shared.dominio.AggregateRoot;
import com.ma.shared.dominio.FacturaId;
import com.ma.shared.dominio.Importe;
import com.ma.shared.dominio.ProveedorId;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Factura extends AggregateRoot {

    private FacturaId id;
    private Comprobante comprobante;
    private FacturaEstado estado;
    private ProveedorId proveedor;
    private Importe total;
    private LocalDate vencimiento;
    private List<LineaFactura> lineas;

    public Factura(FacturaId id, Comprobante comprobante, ProveedorId proveedor, LocalDate vencimiento, Importe total) {
        this.id = id;
        this.comprobante = comprobante;
        this.estado = FacturaEstado.PENDIENTE;
        this.proveedor = proveedor;
        this.vencimiento = vencimiento;
        this.total = total;
        this.lineas = new ArrayList<>();
    }

    public void crearLinea(LineaFacturaId lineaFacturaId, Concepto concepto, Importe importe) {
        validarImporte(importe);
        LineaFactura linea = new LineaFactura(lineaFacturaId, concepto, importe);
        lineas.add(linea);
        total = sumarImportes(lineas);
    }

    public void aprobar() {
        if (estado != FacturaEstado.PENDIENTE) {
            throw new IllegalArgumentException("La factura no está pendiente");
        }
        estado = FacturaEstado.APROBADA;
        FacturaAprobada evento = new FacturaAprobada(id.getId(), estado.name(), vencimiento,
                total, proveedor.getId());
        record(evento);
    }

    private Importe sumarImportes(List<LineaFactura> lista) {
        Importe resultado = Importe.cero(total.getMoneda());
        return lista.stream().map(LineaFactura::getImporte).reduce(resultado, Importe::sumar);
    }

    private void validarImporte(Importe importe) {
        if (total.getMoneda() != importe.getMoneda()) {
            throw new IllegalArgumentException("Moneda no coincide");
        }
    }

    public FacturaId getId() {
        return id;
    }

    public Comprobante getComprobante() {
        return comprobante;
    }

    public ProveedorId getProveedor() {
        return proveedor;
    }

    public FacturaEstado getEstado() {
        return estado;
    }

    public Importe getTotal() {
        return total;
    }

    public LocalDate getVencimiento() {
        return vencimiento;
    }

    public List<LineaFactura> getLineas() {
        return lineas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Factura)) return false;
        Factura factura = (Factura) o;
        return Objects.equals(id, factura.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
